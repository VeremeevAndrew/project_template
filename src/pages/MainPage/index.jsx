// @flow
import * as React from 'react';
import Button     from '../../Components/Button';

type IncomingPropsType = {};
type OwnPropsType = {};

type PropsType = {|
    ...IncomingPropsType,
    ...OwnPropsType
|};

// eslint-disable-next-line
export default (props: PropsType): React.Node => {
    return (
        <div>
            <Button text="Log in" status="default" buttonLabel="Button" />
        </div>);
};
