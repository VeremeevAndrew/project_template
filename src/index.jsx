// @flow
import * as React          from 'react';
import { render }          from 'react-dom';
import { Provider }        from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { renderRoutes }    from 'react-router-config';
import configureStore      from 't-store';
import history             from 't-history';
import initialState        from 't-config/initialState';
import routes              from 't-config/routes';
import rootReducer         from 't-reducers';
import rootSaga            from 't-saga';

const store = configureStore(rootReducer, rootSaga, history, initialState);

// $FlowIgnore
if (module.hot) {
    module.hot.accept();
}

class App extends React.Component<any> {
    render(): React.Node {
        return (
            <Provider store={ store }>
                <ConnectedRouter history={ history }>
                    { renderRoutes(routes) }
                </ConnectedRouter>
            </Provider>);
    }
}


render(<App />, (document.getElementById('root'): any));
