import React            from 'react';
import { renderRoutes } from 'react-router-config';
import { Link }         from 'react-router-dom';

export default props => (
    <div>
        <ul>
            {
                props.route.routes.map((route, i) => (
                    <li key={ i }>
                        <Link to={ route.path }>{ route.path }</Link>
                    </li>))
            }
        </ul>
        { renderRoutes(props.route.routes) }
    </div>);

