import { put } from 'redux-saga/effects';

export default function* () {
    yield put({ type: 'READY' });
}
