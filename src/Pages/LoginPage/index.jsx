// @flow
import * as React           from 'react';
import { Field, reduxForm } from 'redux-form';

type IncomingPropsType = {};
type OwnPropsType = {};

type PropsType = {|
    ...IncomingPropsType,
    ...OwnPropsType
|};

// eslint-disable-next-line
const LoginPage = ({}: PropsType): React.Node => (
    <div>
        <span>Login page</span>
        <form onSubmit={ () => null }>
            <Field name="login" component="input" type="text" />
            <Field name="password" component="input" type="text" />
        </form>
    </div>);

export default reduxForm({
    form: 'login'
})(LoginPage);

