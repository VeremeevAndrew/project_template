// @flow
import * as React from 'react';

type IncomingPropsType = {};
type OwnPropsType = {};

type PropsType = {|
    ...IncomingPropsType,
    ...OwnPropsType
|};

// eslint-disable-next-line
export default ({}: PropsType): React.Node => <div>Signup page</div>;
