import LoginPage  from '../Pages/LoginPage';
import SignupPage from '../Pages/SignupPage';
import MainPage   from '../Pages/MainPage';
import Root       from '../Root.js';

export default [ {
    component: Root,
    routes   : [ {
        path     : '/',
        exact    : true,
        component: MainPage
    }, {
        path     : '/login',
        exact    : true,
        component: LoginPage
    }, {
        path     : '/signup',
        exact    : true,
        component: SignupPage
    } ]
} ];
