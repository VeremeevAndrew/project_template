// @flow

import type { StylesType } from 't-types/JssStyle';

export type PropsType = {|
    +text: string,
    +buttonLabel: string,
    +status: "default" | "error" | "success",
    ...StylesType
|}
