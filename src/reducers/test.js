import initialState from '../Config/initialState';

export default (state = initialState, action) => {
    switch (action.type) {
        case 'TEST': {
            return {
                ...action.payload
            };
        }

        default: return state;
    }
};
