import { combineReducers }        from 'redux';
import { routerReducer }          from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import test                       from './test';

export default combineReducers({
    routing: routerReducer,
    test,
    forms  : formReducer
});
