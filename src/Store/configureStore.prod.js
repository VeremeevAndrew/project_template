import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware                      from 'redux-saga';
import { routerMiddleware }                      from 'react-router-redux';

export default function configureStore(rootReducer, rootSaga, history, initialState) {
    const sagaMiddleware = createSagaMiddleware();

    const store          = createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(sagaMiddleware, routerMiddleware(history)))
    );

    sagaMiddleware.run(rootSaga);

    return store;
}
