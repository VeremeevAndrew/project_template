// @flow

type ClassType<T> = {|
    +classes: T
|};

export type StylesType = ClassType<{|
    [key: string]: {
        [key: string]: number | string
    }
|}>;
