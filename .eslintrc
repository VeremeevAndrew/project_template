{
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true,
            "classes": true,
            "defaultParams": true
        }
    },
    "extends": [
        "airbnb-base",
        "plugin:flowtype/recommended",
        "plugin:varspacing/recommended",
        "plugin:react/recommended"
    ],
    "rules": {
        "class-methods-use-this": 0,
        "react/jsx-indent": [
            2,
            4
        ],
        "react/jsx-indent-props": [
            2,
            4
        ],
        "react/jsx-equals-spacing": 0,
        "react/display-name": 0,
        "react/forbid-prop-types": 0,
        "react/jsx-boolean-value": 1,
        "react/jsx-closing-bracket-location": 2,
        "react/jsx-curly-spacing": [
            2,
            "always"
        ],
        "react/jsx-handler-names": 1,
        "react/jsx-key": 1,
        "max-len": [
            "error",
            {
                "code": 140
            }
        ],
        "react/jsx-max-props-per-line": 0,
        "react/jsx-no-bind": 0,
        "react/jsx-no-duplicate-props": 1,
        "react/jsx-no-literals": 0,
        "react/jsx-no-undef": 1,
        "react/jsx-pascal-case": 1,
        "arrow-parens": 0,
        "react/jsx-sort-prop-types": 0,
        "react/jsx-sort-props": 0,
        "react/jsx-uses-react": 1,
        "react/jsx-uses-vars": 1,
        "react/no-danger": 1,
        "react/no-deprecated": 1,
        "react/no-did-mount-set-state": 1,
        "react/no-did-update-set-state": 1,
        "react/no-direct-mutation-state": 1,
        "react/no-is-mounted": 1,
        "react/no-multi-comp": 0,
        "react/no-set-state": 1,
        "react/no-string-refs": 0,
        "react/no-unknown-property": 1,
        "react/prefer-es6-class": 1,
        "react/prop-types": 0,
        "react/react-in-jsx-scope": 1,
        "react/require-extension": 0,
        "react/self-closing-comp": 1,
        "react/sort-comp": 1,
        "react/wrap-multilines": 0,
        "import/prefer-default-export": 0,
        "import/no-extraneous-dependencies": 0,
        "import/no-unresolved": 0,
        "import/extensions": 0,
        "indent": [
            2,
            4,
            {
                "SwitchCase": 1
            }
        ],
        "consistent-return": 0,
        "comma-dangle": [
            2,
            "only-multiline"
        ],
        "template-curly-spacing": [
            2,
            "always"
        ],
        "object-curly-spacing": [
            2,
            "always"
        ],
        "array-bracket-spacing": [
            2,
            "always"
        ],
        "no-multi-spaces": 0,
        "one-var": 0,
        "func-names": 0,
        "function-paren-newline": 0,
        "lines-around-comment": [
            2,
            {
                "beforeBlockComment": true,
                "afterBlockComment": true,
                "beforeLineComment": true,
                "afterLineComment": false,
                "allowBlockStart": true,
                "allowBlockEnd": true,
                "allowObjectStart": true,
                "allowObjectEnd": true,
                "allowArrayStart": true,
                "allowArrayEnd": true
            }
        ],
        "key-spacing": [
            2,
            {
                "align": "colon"
            }
        ],
        "arca/import-align": 2,
        "no-underscore-dangle": 0
    },
    "plugins": [
        "flowtype",
        "arca",
        "varspacing",
        "react"
    ]
}