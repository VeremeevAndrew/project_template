const { config, webpackConfig } = require('./webpack.common');
const webpack                   = require('webpack');
const merge                     = require('webpack-merge');

module.exports = merge(webpackConfig, {
    mode   : 'development',
    devtool: 'inline-source-map',
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
    ],
    devServer: {
        host              : config.devServer.host,
        port              : config.devServer.port,
        historyApiFallback: true,
        hot               : true,
        inline            : true,
        progress          : true,
        stats             : {
            assets      : false,
            colors      : true,
            version     : false,
            hash        : false,
            timings     : false,
            chunks      : false,
            chunkModules: false
        }
    }
});
