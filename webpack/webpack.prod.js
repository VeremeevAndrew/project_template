const { webpackConfig } = require('./webpack.common');
const webpack           = require('webpack');
const merge             = require('webpack-merge');

module.exports = merge(webpackConfig, {
    mode: 'production',

    optimization: {
        minimize: false
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        })
    ]
});
