const
    HtmlWebpackPlugin  = require('html-webpack-plugin'),
    CleanWebpackPlugin = require('clean-webpack-plugin'),

    // MiniCssExtractPlugin = require('mini-css-extract-plugin'),
    path               = require('path');

const config = {
    path: {
        app   : path.resolve(__dirname, '../src'),
        dist  : path.resolve(__dirname, '../dist'),
        public: path.resolve(__dirname, '../public')
    },
    devServer: {
        host: '0.0.0.0',
        port: 3000
    }
};

const webpackConfig = {
    entry: [
        'webpack/hot/only-dev-server',
        `${ config.path.app }/index.jsx`
    ],
    output: {
        path      : config.path.dist,
        filename  : 'bundle.js',
        publicPath: '/'
    },
    module: {
        rules: [
        //     {
        //     test: /\.scss$/,
        //     use : [
        //         MiniCssExtractPlugin.loader,
        //         {
        //             loader : 'css-loader',
        //             options: {
        //                 minimize: {
        //                     safe: true
        //                 }
        //             }
        //         },
        //         {
        //             loader: 'postcss-loader'
        //         },
        //         {
        //             loader : 'sass-loader',
        //             options: {}
        //         }
        //     ]
        // },

            {
                test   : /\.(js|jsx|mjs)?$/,
                exclude: /(node_modules\/(?!(container-ioc|parse-domain|webpack-dev-server)\/).*|flow-typed|.history)/,
                loader : 'babel-loader'
            }, {
                test  : /\.json$/,
                loader: 'json-loader'
            } ]
    },
    plugins: [
        new CleanWebpackPlugin('dist'),

        // new MiniCssExtractPlugin({
        //     filename: 'styles.css'
        // }),

        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: `${ config.path.public }/index.html`,
            minify  : {
                collapseWhitespace: true
            }
        })
    ],
    resolve: {
        alias: {
            't-components': path.resolve(config.path.app, 'Components'),
            't-actions'   : path.resolve(config.path.app, 'Actions'),
            't-config'    : path.resolve(config.path.app, 'Config'),
            't-containers': path.resolve(config.path.app, 'Containers'),
            't-history'   : path.resolve(config.path.app, 'History'),
            't-hoc'       : path.resolve(config.path.app, 'HOC'),
            't-pages'     : path.resolve(config.path.app, 'Pages'),
            't-reducers'  : path.resolve(config.path.app, 'Reducers'),
            't-saga'      : path.resolve(config.path.app, 'Saga'),
            't-store'     : path.resolve(config.path.app, 'Store'),
            't-types'     : path.resolve(config.path.app, 'Types')
        },
        extensions: [ '.js', '.json', '.jsx', '.jss' ]
    },
};

module.exports = { webpackConfig, config };
